-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 05. Mrz 2021 um 11:30
-- Server-Version: 10.4.11-MariaDB
-- PHP-Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `studentsdb`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `classrooms`
--

CREATE TABLE `classrooms` (
                              `id` int(11) NOT NULL,
                              `label` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `classrooms`
--

INSERT INTO `classrooms` (`id`, `label`) VALUES
(1, '1IT'),
(2, '2IT'),
(3, '3IT'),
(4, '4IT');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `students`
--

CREATE TABLE `students` (
                            `id` int(11) NOT NULL,
                            `firstname` varchar(45) DEFAULT NULL,
                            `lastname` varchar(45) DEFAULT NULL,
                            `rollno` int(11) DEFAULT NULL,
                            `classrooms_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `classrooms`
--
ALTER TABLE `classrooms`
    ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `students`
--
ALTER TABLE `students`
    ADD PRIMARY KEY (`id`,`classrooms_id`),
    ADD KEY `fk_students_classrooms_idx` (`classrooms_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `classrooms`
--
ALTER TABLE `classrooms`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `students`
--
ALTER TABLE `students`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `students`
--
ALTER TABLE `students`
    ADD CONSTRAINT `fk_students_classrooms` FOREIGN KEY (`classrooms_id`) REFERENCES `classrooms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
