module at.hakimst {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires mysql.connector.java;

    opens at.hakimst.controller to javafx.fxml;
    exports at.hakimst;
}