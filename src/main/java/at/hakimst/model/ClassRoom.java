package at.hakimst.model;

import java.util.Objects;

/**
 * Represents a class room including an id and label.
 *
 */
public class ClassRoom {
    private Long id;
    private String label;

    public ClassRoom(Long id, String label) {
        this.id = id;
        this.label = label;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClassRoom classRoom = (ClassRoom) o;
        return Objects.equals(id, classRoom.id) && Objects.equals(label, classRoom.label);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, label);
    }
}
