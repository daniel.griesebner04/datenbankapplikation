package at.hakimst.model;

import java.util.Objects;

/**
 * Represents a student where id is the artificial id (resulting from the db) and rollNo is the domain id.
 *
 */
public class Student {
    private Long id;
    private String firstName;
    private String lastName;
    private int rollNo;
    private ClassRoom classRoom;

    public Student(String firstName, String lastName, int rollNo, ClassRoom classRoom){
        this(null, firstName, lastName, rollNo, classRoom);
    }

    public Student(Long id, String firstName, String lastName, int rollNo, ClassRoom classRoom){
        super();
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.rollNo = rollNo;
        this.classRoom = classRoom;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getRollNo() {
        return rollNo;
    }

    public ClassRoom getClassRoom() {
        return classRoom;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }

    public void setClassRoom(ClassRoom classRoom) {
        this.classRoom = classRoom;
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", rollNo=" + rollNo + ", Classroom: " + classRoom.getLabel();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return rollNo == student.rollNo && Objects.equals(id, student.id) && Objects.equals(firstName, student.firstName) && Objects.equals(lastName, student.lastName) && Objects.equals(classRoom, student.classRoom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, rollNo, classRoom);
    }
}
