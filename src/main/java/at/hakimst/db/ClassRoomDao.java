package at.hakimst.db;

import at.hakimst.model.ClassRoom;
import at.hakimst.model.Student;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/** DAO implementation for student class */
public class ClassRoomDao implements IDao<ClassRoom> {
    private final static String TABLE_NAME = "classrooms";

    public ClassRoomDao(){
        super();
    }

    @Override
    public List<ClassRoom> getAllObjects() {
        List<ClassRoom> classRoomList = new ArrayList();
        String sql = "SELECT * FROM " + TABLE_NAME;
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            while (set.next()) {
                ClassRoom classRoom = new ClassRoom(set.getLong(1), set.getString(2));
                classRoomList.add(classRoom);
            }
            return classRoomList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return classRoomList;
    }

    public List<String> getAllClassLabels(){
        List<String> classLabels = new ArrayList<>();
        List<ClassRoom> classRoomList = getAllObjects();
        for(ClassRoom classRoom : classRoomList){
            classLabels.add(classRoom.getLabel());
        }
        return classLabels;
    }

    @Override
    public ClassRoom getObject(int id) {
        String sql = "SELECT * FROM " + TABLE_NAME + " WHERE id = ?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1,id);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            set.next();
            ClassRoom classRoom =  new ClassRoom((long) id, set.getString(2));
            return classRoom;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addObject(ClassRoom classRoom) {
        try {
            String insertSql = "INSERT INTO " + TABLE_NAME + "(label) VALUES (?)";
            PreparedStatement ps = DBManager.getConnection().prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, classRoom.getLabel());
            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            keys.next();
            classRoom.setId((long) keys.getInt(1));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateObject(ClassRoom classRoom) {
        try {
            String insertSql = "UPDATE " + TABLE_NAME + " SET label = ? WHERE ID = ?";
            PreparedStatement ps = DBManager.getConnection().prepareStatement(insertSql);
            ps.setString(1, classRoom.getLabel());
            ps.setLong(2, classRoom.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void deleteObject(int id) {
        String sql = "DELETE FROM " + TABLE_NAME + " WHERE id = ?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1,id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
