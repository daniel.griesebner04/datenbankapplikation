package at.hakimst.db;

import at.hakimst.model.ClassRoom;
import at.hakimst.model.Student;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/** DAO implementation for student class */
public class StudentDao implements IDao<Student> {
    private final static String TABLE_NAME = "students";
    private ClassRoomDao classRoomDao;

    public StudentDao(){
        super();
        classRoomDao = new ClassRoomDao();
    }

    @Override
    public List<Student> getAllObjects() {
        List<Student> studentList = new ArrayList();
        String sql = "SELECT * FROM " + TABLE_NAME;
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            while (set.next()) {
                ClassRoom classRoom = classRoomDao.getObject(set.getInt(5));
                Student student = new Student(set.getLong(1), set.getString(2), set.getString(3), set.getInt(4), classRoom );
                studentList.add(student);
            }
            return studentList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return studentList;
    }


    /**
     * Returns a student with a specified id from the databse
     * @param id the student's id
     * @return student object
     */
    @Override
    public Student getObject(int id) {
        String sql = "SELECT * FROM " + TABLE_NAME + " WHERE id = ?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1,id);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            set.next();
            ClassRoom classRoom = classRoomDao.getObject(set.getInt(5));
            Student student =  new Student((long) id, set.getString(2), set.getString(3), set.getInt(4), classRoom);
            return student;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Creates a student in the database
     * @param student the student to create
     */
    @Override
    public void addObject(Student student) {
        try {
            String insertSql = "INSERT INTO " + TABLE_NAME + "(firstname, lastname, rollno,classrooms_id) VALUES (?,?,?, ?)";
            PreparedStatement ps = DBManager.getConnection().prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, student.getFirstName());
            ps.setString(2, student.getLastName());
            ps.setInt(3, student.getRollNo());
            ps.setLong(4, student.getClassRoom().getId());
            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            keys.next();
            student.setId((long) keys.getInt(1));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update the specified student
     * @param student the student to update
     */
    @Override
    public void updateObject(Student student) {
        try {
            String insertSql = "UPDATE " + TABLE_NAME + " SET firstname = ?, lastname = ?, rollno = ?, classroom_id = ? WHERE ID = ?";
            PreparedStatement ps = DBManager.getConnection().prepareStatement(insertSql);
            ps.setString(1, student.getFirstName());
            ps.setString(2, student.getLastName());
            ps.setInt(3, student.getRollNo());
            ps.setLong(4, student.getClassRoom().getId());
            ps.setLong(5, student.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Removes the specified student from the database
     * @param id student to remove
     */
    @Override
    public void deleteObject(int id) {
        String sql = "DELETE FROM " + TABLE_NAME + " WHERE id = ?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1,id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
