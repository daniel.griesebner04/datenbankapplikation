package at.hakimst.db;

import java.util.List;

/**
 * Generic interface for CRUD operations.
 * @param <E> type
 */
public interface IDao<E> {

    List<E> getAllObjects();

    E getObject(int id);

    void addObject(E e);

    void updateObject(E e);

    void deleteObject(int id);


}
