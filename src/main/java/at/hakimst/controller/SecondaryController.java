package at.hakimst.controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import at.hakimst.App;
import at.hakimst.db.StudentDao;
import at.hakimst.model.Student;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

public class SecondaryController {
    StudentDao studentDao;
    @FXML
    TextArea textArea;


    public void initialize(){
        studentDao = new StudentDao();
        List<Student> studentList = studentDao.getAllObjects();
        String listString = studentList.stream().map(Object::toString)
                .collect(Collectors.joining("\n"));
        textArea.setText(listString);
    }

    @FXML
    private void switchToPrimary() throws IOException {
        App.setRoot("primary");
    }
}