package at.hakimst.controller;

import java.io.IOException;

import at.hakimst.App;
import at.hakimst.db.ClassRoomDao;
import at.hakimst.db.StudentDao;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class PrimaryController {
    StudentDao studentDao;
    ClassRoomDao classRoomDao;

    @FXML
    TextField textFirstName, textLastName, textRollNo;

    @FXML
    ComboBox comboClassRoom;

    public void initialize() {
        studentDao = new StudentDao();
        classRoomDao = new ClassRoomDao();
        comboClassRoom.getItems().addAll(classRoomDao.getAllClassLabels());
    }

    @FXML
    private void switchToSecondary() throws IOException {
        App.setRoot("secondary");
    }

    @FXML
    public void addStudent() {
//        Student student = new Student(textFirstName.getText(), textLastName.getText(), Integer.valueOf(textRollNo.getText()));
//        studentDao.addObject(student);
        clearFields();
    }

    private void clearFields(){
        textFirstName.clear();
        textLastName.clear();
        textRollNo.clear();
    }
}
